#!/usr/bin/env ruby
# dogs.rb
# Dog street fighting simulator :)

class Dog

  attr_accessor :name, :hp

  def initialize(name)
    @name = name
    @hp = 10
  end

  def alive?
    self.hp > 0 ? true : false
  end

  def barks
    puts "#{self.name} barks."
  end

  def snarls
    puts "#{self.name} snarls."
  end

  def attacks(x)
    dmg = rand(5) + 1
    x.hp -= dmg
    puts "#{self.name} attacks #{x.name} for #{dmg} damage."
  end
end


def fight(first, second)
  swap = rand(2)
  if swap == 1 then first,second = second,first end

  10.times do
    action = rand(3)
    case action
    when 0
      first.barks
    when 1
      first.snarls
    when 2
      first.attacks(second)
    end

    if !second.alive?
      puts second.name + " dies."
      break
    end
    first,second = second,first
    sleep 1
  end

  if first.alive? and second.alive?
    if first.hp < second.hp
      puts "#{first.name} flees the scene." 
    elsif second.hp < first.hp
      puts "#{second.name} flees the scene." 
    else
      puts "Both dogs flee in the opposite directions." 
    end
  end
end

names = Array["Rex","Fluffy","Bobik","Zhuchka","Adolf","Alma","Alfa","Rusty","Belka","Strelka","Fatso","Grumpy","Lady","Lassie","Raspberry","Cookie","Red","Old Dog","Beast","Nosie","Tails","Merry Sue","Flea Market","Drake","Grasshopper","Kirk","Jason","Malmsteen","Rover","Phteven","Bella","Luna","Lucy","Daisy","Zoe","Lily","Lola","Bailey","Stella","Molly","Cooper","Buddy","Milo","Fido","Seth"]

dogs = Array.new
1.times do
  2.times { dogs << Dog.new(name = names[rand(names.length)]) }
  fight(dogs[0], dogs[1]) 
  dogs = []
  puts
end

